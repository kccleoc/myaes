package myaes

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"errors"
	"io"
)

// Encrypt bytes to cipher bytes using AES
func Encrypt(in, key []byte) ([]byte, error) {

	// sha256(Key) to make sure key must be 32 bytes
	secret := sha256.Sum256(key)

	// Create the AES cipher
	block, err := aes.NewCipher(secret[:])
	if err != nil {
		return nil, err
	}

	// Empty array of 32 + in data length
	// Include the IV at the beginning
	cipherBytes := make([]byte, aes.BlockSize+len(in))

	// Slice of first 16 bytes
	iv := cipherBytes[:aes.BlockSize]

	// Write 32 rand bytes to fill iv
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, err
	}

	// Return an encrypted stream
	stream := cipher.NewCFBEncrypter(block, iv)

	// Encrypt in bytes to cipherBytes
	stream.XORKeyStream(cipherBytes[aes.BlockSize:], in)

	return cipherBytes, nil
}

// Decrypt cipher bytes using AES
func Decrypt(cipherByte, key []byte) ([]byte, error) {

	// Key
	secret := sha256.Sum256(key)

	// Create the AES cipher
	block, err := aes.NewCipher(secret[:])
	if err != nil {
		return nil, err
	}

	// Before even testing the decryption,
	// if the text is too small, then it is incorrect
	if len(cipherByte) < aes.BlockSize {
		return nil, errors.New("cipher bytes too short to decrypt")
	}

	// Get the 16 byte IV
	iv := cipherByte[:aes.BlockSize]

	// Remove the IV from the ciphertext
	cipherByte = cipherByte[aes.BlockSize:]

	// Return a decrypted stream
	stream := cipher.NewCFBDecrypter(block, iv)

	// Decrypt bytes from cipherByte
	stream.XORKeyStream(cipherByte, cipherByte)

	return cipherByte, nil
}
