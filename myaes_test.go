package myaes

import (
	"testing"
)

func TestEncrypt(t *testing.T) {
	in := []byte("這是我的秘密，no?")
	secret := []byte("ニホン")
	ciText, _ := Encrypt(in, secret)
	out, _ := Decrypt(ciText, secret)
	if string(in) != string(out) {
		t.Logf("in: %s\nout: %s", in, out)
		t.Error("Failed")
	}
}
